class ContactsController < ApplicationController
  def create
    @contact = Contact.new(params[:contact])
    if @contact.save
      render json: @contact
    else
      render json: @contact.errors.full_messages, status: :unprocessable_entity
    end
  end

  def destroy
    @contact = Contact.find(params[:id])
    @contact.destroy
    render json: @contact
  end

  def index
    @contacts = self.class.contacts_for_user_id(params[:user_id])

    render json: @contacts
  end

  def self.contacts_for_user_id(user_id)
    Contact.find_by_sql [<<-SQL, user_id, user_id]
      SELECT DISTINCT c.*
      FROM contacts c
      LEFT OUTER JOIN contact_shares cs ON c.id = cs.contact_id
      WHERE c.user_id = ? OR cs.user_id = ?
    SQL
  end

  def favorites

  end

  def show
    @contact = Contact.find(params[:id])
    render json: @contact
  end

  def update
    @contact = Contact.find(params[:id])
    if @contact.update_attributes(params[:contact])
      render json: @contact
    else
      render json: @contact.errors.full_messages, status: :unprocessable_entity
    end
  end
end
