class ContactGroupsController < ApplicationController

  def create
    @contact_group = ContactGroup.new(params[:contact_group])

    if @contact_group.save
      render json: @contact_group
    else
      render json: @contact_group.errors.full_message, status: :unprocessable_entity
    end
  end

  def destroy
    @contact_group = ContactGroup.find(params[:id])

    @contact_group.destroy

    render json: @contact_group
  end

  def index
    render json: ContactGroup.all
  end

  def show
    @contact_group = ContactGroup.find(params[:id])

    render json: @contact_group
  end

  def update
    @contact_group = ContactGroup.find(params[:id])

    if @contact_group.update_attributes(params[:contact_group])
      render json: @contact_group
    else
      render json: @contact_group.errors.full_message, status: :unprocessable_entity
    end
  end

end
