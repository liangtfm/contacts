class ContactGroupMembershipsController < ApplicationController

  def create
    @contact_group_membership = ContactGroupMembership.new(params[:contact_group_membership])

    if @contact_group_membership.save
      render json: @contact_group_membership
    else
      render json: @contact_group_membership.errors.full_message, status: :unprocessable_entity
    end
  end

  def destroy
    @contact_group_membership = ContactGroupMembership.find(params[:id])

    @contact_group_membership.destroy

    render json: @contact_group_membership
  end

end
