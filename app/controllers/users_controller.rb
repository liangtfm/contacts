class UsersController < ApplicationController

  def create
    @user = User.new(params[:user])

    if @user.save
      render :json => @user
    else
      render :json => @user.errors.full_messages, status: :unprocessable_entity
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    render :json => @user
  end

  def index
    render :json => User.all
  end

  def show
    @user = User.find(params[:id])
    render :json => @user
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      render json: @user
    else
      render json: @user.update_attributes(params[:user]).errors.full_messages,
        status: :unprocessable_entity
    end
  end

end
