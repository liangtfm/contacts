class Contact < ActiveRecord::Base
  attr_accessible :email, :name, :user
  validates :email, :presence => true, :uniqueness => true

  belongs_to :user,
  primary_key: :id,
  foreign_key: :user_id,
  class_name: "User"

  has_many :shared_users,
  through: :contact_shares,
  source: :user

  has_many :contact_shares,
  class_name: "ContactShare",
  foreign_key: :contact_id,
  primary_key: :id

  has_many :contact_groups,
  through: :contact_group_memberships,
  source: :contact_group

  has_many :contact_group_memberships,
  class_name: "ContactGroupMembership",
  foreign_key: :contact_id,
  primary_key: :id

end
