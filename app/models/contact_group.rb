class ContactGroup < ActiveRecord::Base
  attr_accessible :user, :name

  belongs_to :user,
  class_name: "User",
  foreign_key: :user_id,
  primary_key: :id

  has_many :contact_group_memberships,
  class_name: "ContactGroupMembership",
  foreign_key: :contact_group_id,
  primary_key: :id

  has_many :contacts,
  through: :contact_group_memberships,
  source: :contact

end
