class ContactGroupMembership < ActiveRecord::Base
  attr_accessible :contact, :contact_group

  belongs_to :contact,
  class_name: "Contact",
  foreign_key: :contact_id,
  primary_key: :id

  belongs_to :contact_group,
  class_name: "ContactGroup",
  foreign_key: :contact_group_id,
  primary_key: :id

end
