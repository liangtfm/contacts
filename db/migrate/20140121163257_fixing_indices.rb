class FixingIndices < ActiveRecord::Migration
  def change
    remove_index :contact_shares, :name => "index_contact_shares_on_contact_id_and_user_id"
    remove_index :contacts, :name => "index_contacts_on_email_and_user_id"

    add_index :contact_shares, :contact_id
    add_index :contact_shares, :user_id
    add_index :contacts, :email, :unique => true
    add_index :contacts, :user_id
  end
end
