class CreateContactGroupMemberships < ActiveRecord::Migration
  def change
    create_table :contact_group_memberships do |t|
      t.integer :contact_group_id
      t.integer :contact_id

      t.timestamps
    end

    add_index :contact_group_memberships, :contact_group_id
    add_index :contact_group_memberships, :contact_id
  end
end
